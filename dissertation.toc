\vspace {-\cftbeforepartskip }
\contentsline {part}{i\hspace {1em}\spacedlowsmallcaps {Introductory material}}{1}{part.1}
\contentsline {chapter}{\numberline {1}\spacedlowsmallcaps {Introduction}}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Contextualization}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Motivation}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Document Structure}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}\spacedlowsmallcaps {State of the art}}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Bugle}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Filter and Statistics Configuration}{7}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Graphic User Interface}{9}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Inner Workings}{11}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Maintenance}{11}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Apitrace}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Inner Workings}{14}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Maintenance}{15}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}GLIntercept}{15}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Logging}{16}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Plugins}{17}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Inner Workings}{19}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Maintenance}{20}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}GLSLDevil/GLSL-Debugger}{21}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Graphic User Interface}{22}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Maintenance}{24}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}VOGL}{24}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Functions and GUI}{25}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Maintenance}{26}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}CodeXL}{26}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Debug Mode}{27}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Profilling Mode}{27}{subsection.2.6.2}
\contentsline {section}{\numberline {2.7}NSight}{27}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Graphics Debugging}{28}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Performance Analysis}{30}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}Debugger Comparisons}{31}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Debugging basic essentials and useful tools}{35}{subsection.2.8.1}
\contentsline {part}{ii\hspace {1em}\spacedlowsmallcaps {Debugging in Nau}}{37}{part.2}
\contentsline {chapter}{\numberline {3}\spacedlowsmallcaps {Adding Debugging Features}}{38}{chapter.3}
\contentsline {section}{\numberline {3.1}Integrating a debugger with Nau}{38}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Changes on GLIntercept}{39}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Changes on Nau}{40}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Changes on composer}{41}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Adding other Debugging Features}{41}{section.3.2}
\contentsline {chapter}{\numberline {4}\spacedlowsmallcaps {A Guide to Nau's debugger}}{43}{chapter.4}
\contentsline {section}{\numberline {4.1}functionlog}{44}{section.4.1}
\contentsline {section}{\numberline {4.2}logperframe}{44}{section.4.2}
\contentsline {section}{\numberline {4.3}errorchecking}{45}{section.4.3}
\contentsline {section}{\numberline {4.4}imagelog}{46}{section.4.4}
\contentsline {section}{\numberline {4.5}framelog}{47}{section.4.5}
\contentsline {section}{\numberline {4.6}timerlog}{48}{section.4.6}
\contentsline {section}{\numberline {4.7}plugins}{48}{section.4.7}
\contentsline {section}{\numberline {4.8}Retrieving OpenGL state}{49}{section.4.8}
\contentsline {section}{\numberline {4.9}Using Composer}{50}{section.4.9}
\contentsline {chapter}{\numberline {5}\spacedlowsmallcaps {Conclusions and future work}}{54}{chapter.5}
\contentsline {section}{\numberline {5.1}Conclusions}{54}{section.5.1}
\contentsline {section}{\numberline {5.2}Prospect for future work}{55}{section.5.2}
\vspace \bigskipamount 
\contentsline {part}{iii\hspace {1em}\spacedlowsmallcaps {Appendices}}{59}{part.3}
\contentsline {chapter}{\numberline {A}\spacedlowsmallcaps {Bugle}}{60}{appendix.A}
\contentsline {section}{\numberline {A.1}Installation}{60}{section.A.1}
\contentsline {section}{\numberline {A.2}Usage and Configuration}{62}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Filter Configuration}{62}{subsection.A.2.1}
\contentsline {subsubsection}{\numberline {A.2.1.1}Statistics configuration}{62}{subsubsection.A.2.1.1}
\contentsline {subsubsection}{\numberline {A.2.1.2}Statistics filterset}{63}{subsubsection.A.2.1.2}
\contentsline {subsubsection}{\numberline {A.2.1.3}Trace and Log filterset}{65}{subsubsection.A.2.1.3}
\contentsline {subsubsection}{\numberline {A.2.1.4}Error checking filtersets}{67}{subsubsection.A.2.1.4}
\contentsline {subsubsection}{\numberline {A.2.1.5}KHR\_Debug filterset}{68}{subsubsection.A.2.1.5}
\contentsline {subsubsection}{\numberline {A.2.1.6}Context attributes and extension override filtersets}{68}{subsubsection.A.2.1.6}
\contentsline {subsubsection}{\numberline {A.2.1.7}showextensions filterset}{70}{subsubsection.A.2.1.7}
\contentsline {subsubsection}{\numberline {A.2.1.8}Compilable C source filterset}{70}{subsubsection.A.2.1.8}
\contentsline {subsubsection}{\numberline {A.2.1.9}Screenshot filterset}{71}{subsubsection.A.2.1.9}
\contentsline {subsubsection}{\numberline {A.2.1.10}eps filterset}{72}{subsubsection.A.2.1.10}
\contentsline {subsubsection}{\numberline {A.2.1.11}frontbuffer filterset}{72}{subsubsection.A.2.1.11}
\contentsline {subsubsection}{\numberline {A.2.1.12}Wireframe filterset}{72}{subsubsection.A.2.1.12}
\contentsline {chapter}{\numberline {B}\spacedlowsmallcaps {Apitrace}}{73}{appendix.B}
\contentsline {section}{\numberline {B.1}Installation}{73}{section.B.1}
\contentsline {section}{\numberline {B.2}Usage and Configuration}{74}{section.B.2}
\contentsline {subsection}{\numberline {B.2.1}Tracing}{74}{subsection.B.2.1}
\contentsline {subsection}{\numberline {B.2.2}Retracing}{74}{subsection.B.2.2}
\contentsline {subsection}{\numberline {B.2.3}Output replay to video}{74}{subsection.B.2.3}
\contentsline {subsection}{\numberline {B.2.4}Trimming trace file}{75}{subsection.B.2.4}
\contentsline {subsection}{\numberline {B.2.5}Profiling trace}{75}{subsection.B.2.5}
\contentsline {chapter}{\numberline {C}\spacedlowsmallcaps {GLIntercept}}{77}{appendix.C}
\contentsline {section}{\numberline {C.1}Installation}{77}{section.C.1}
\contentsline {section}{\numberline {C.2}Usage and Configuration}{77}{section.C.2}
\contentsline {subsection}{\numberline {C.2.1}Tracing}{77}{subsection.C.2.1}
\contentsline {subsection}{\numberline {C.2.2}Frame Logging}{80}{subsection.C.2.2}
\contentsline {subsection}{\numberline {C.2.3}Shader Editor}{80}{subsection.C.2.3}
\contentsline {subsection}{\numberline {C.2.4}ARB\_debug\_output Logging}{81}{subsection.C.2.4}
\contentsline {subsection}{\numberline {C.2.5}Extension override}{81}{subsection.C.2.5}
\contentsline {subsection}{\numberline {C.2.6}Function statistics}{83}{subsection.C.2.6}
\contentsline {chapter}{\numberline {D}\spacedlowsmallcaps {GLSLDevil/GLSL-Debugger}}{85}{appendix.D}
\contentsline {section}{\numberline {D.1}Installation}{85}{section.D.1}
\contentsline {section}{\numberline {D.2}Usage and Configuration}{85}{section.D.2}
\contentsline {subsection}{\numberline {D.2.1}Graphic User Interface}{85}{subsection.D.2.1}
\contentsline {subsubsection}{\numberline {D.2.1.1}GL Trace}{87}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Run}{87}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Step}{87}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Skip}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Edit}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Jump to next Draw Call}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Jump to next Shader Switch (F6)}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Jump to next User Defined OpenGL}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Run (F8)}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Stop (Alt+Break)}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Halt on Error}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Disable GL Trace}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Show GL Trace settings}{88}{subsubsection.D.2.1.1}
\contentsline {paragraph}{Save GL Trace}{89}{subsubsection.D.2.1.1}
\contentsline {subsubsection}{\numberline {D.2.1.2}Shader}{89}{subsubsection.D.2.1.2}
\contentsline {paragraph}{Debug Shader}{89}{subsubsection.D.2.1.2}
\contentsline {paragraph}{Reset Debug Session (Ctrl+Shift+F5)}{89}{subsubsection.D.2.1.2}
\contentsline {paragraph}{Step (F11)}{89}{subsubsection.D.2.1.2}
\contentsline {paragraph}{Step Over (F10)}{89}{subsubsection.D.2.1.2}
\contentsline {paragraph}{Edit Per-Fragment Option}{89}{subsubsection.D.2.1.2}
\contentsline {subsubsection}{\numberline {D.2.1.3}GL Trace Statistics}{89}{subsubsection.D.2.1.3}
\contentsline {paragraph}{Combo Boxes}{89}{subsubsection.D.2.1.3}
\contentsline {paragraph}{Tabs}{90}{subsubsection.D.2.1.3}
\contentsline {subsubsection}{\numberline {D.2.1.4}GL Buffer View}{90}{subsubsection.D.2.1.4}
\contentsline {paragraph}{Capture}{90}{subsubsection.D.2.1.4}
\contentsline {paragraph}{Automatic Capture}{90}{subsubsection.D.2.1.4}
\contentsline {paragraph}{Save as image}{90}{subsubsection.D.2.1.4}
\contentsline {subsection}{\numberline {D.2.2}Shader Variables and Watch}{91}{subsection.D.2.2}
\contentsline {chapter}{\numberline {E}\spacedlowsmallcaps {VOGL}}{93}{appendix.E}
\contentsline {section}{\numberline {E.1}Installation}{93}{section.E.1}
\contentsline {section}{\numberline {E.2}Usage and Configuration}{94}{section.E.2}
\contentsline {subsection}{\numberline {E.2.1}Copying the DLL}{94}{subsection.E.2.1}
\contentsline {subsection}{\numberline {E.2.2}VOGL gui}{94}{subsection.E.2.2}
\contentsline {subsection}{\numberline {E.2.3}Creating the trace file}{95}{subsection.E.2.3}
\contentsline {subsection}{\numberline {E.2.4}Trimming a trace file}{96}{subsection.E.2.4}
\contentsline {subsection}{\numberline {E.2.5}Replaying a trace file}{96}{subsection.E.2.5}
\contentsline {subsection}{\numberline {E.2.6}Interactive replaying a trace file}{97}{subsection.E.2.6}
\contentsline {subsection}{\numberline {E.2.7}Realtime editing and replaying a trace file}{97}{subsection.E.2.7}
\contentsline {subsection}{\numberline {E.2.8}Converting a Apitrace trace file}{97}{subsection.E.2.8}
\contentsline {subsection}{\numberline {E.2.9}Dump images from a trace file}{98}{subsection.E.2.9}
\contentsline {subsection}{\numberline {E.2.10}Get statistics from a trace file}{98}{subsection.E.2.10}
\contentsline {subsection}{\numberline {E.2.11}Finding in a trace file}{101}{subsection.E.2.11}
